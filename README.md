# Migrateur de Données - TD Bonus

Ce projet a été réalisé par Geoffrey Pierre et Adam Henchiri en binôme dans le cadre du TD bonus. 
Nous avons appliqué l'architecture recommandée pour développer une application de migration de données, permettant de transférer des données de n'importe quelle base de données vers une autre.

## Fonctionnalités

- Migration de données entre différentes bases de données.
- Implémentation des bases de données Cassandra et MongoDB.

## Comment utiliser

1. Clonez ce dépôt sur votre machine locale.
```bash
git clone https://gitlabinfo.iutmontp.univ-montp2.fr/pierreg/bd_cassandra_mongo.git
cd bd_cassandra_mongo
```
2. Lancez le main du projet.

# Pour ajoutez une nouvelle base de données
1. Créez un dossier du nom de votre bdd dans le répertoire src/stockage pour la nouvelle base de données.
Ajoutez deux classes :
2. Une classe pour la connexion à cette base de données avec vos identifiants.
3. Une classe de migration qui implémente FabriqueMigrateur (vous devez bien évidemment completer les méthodes nécessaires).