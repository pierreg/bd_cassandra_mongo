package metier;

import java.util.ArrayList;

//A compléter!
public class Club {

    //Attributs (et getters/setters) à définir.
    private String id_club;
    private String nom_club;
    private String ville_club;

    public Club(String id_club, String nom_club, String ville_club) {
        this.id_club = id_club;
        this.nom_club = nom_club;
        this.ville_club = ville_club;
    }

    @Override
    public String toString() {
        return " Du club n°" + id_club +
                ", " + nom_club + '\'' +
                "à " + ville_club;
    }

    public String getId_club() {
        return id_club;
    }

    public void setId_club(String id_club) {
        this.id_club = id_club;
    }

    public String getNom_club() {
        return nom_club;
    }

    public void setNom_club(String nom_club) {
        this.nom_club = nom_club;
    }

    public String getVille_club() {
        return ville_club;
    }

    public void setVille_club(String ville_club) {
        this.ville_club = ville_club;
    }
}
