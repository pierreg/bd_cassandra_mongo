package metier;

//A completer !

public class Tournoi {

    private String id_tournois;
    private String nom_tournois;

    public Tournoi(String id_tournois, String nom_tournois) {
        this.id_tournois = id_tournois;
        this.nom_tournois = nom_tournois;
    }

    @Override
    public String toString() {
        return "Tournoi n°" + id_tournois +
                ", nommé " + nom_tournois;
    }

    public String getId_tournois() {
        return id_tournois;
    }

    public void setId_tournois(String id_tournois) {
        this.id_tournois = id_tournois;
    }

    public String getNom_tournois() {
        return nom_tournois;
    }

    public void setNom_tournois(String nom_tournois) {
        this.nom_tournois = nom_tournois;
    }
}
