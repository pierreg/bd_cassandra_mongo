package metier;

//A completer !

public class Resultat {

    //Attributs (et getters/setters) à définir.
    private int classement;

    private Tournoi tournoi;

    public Resultat(int classement, Tournoi tournoi) {
        this.classement = classement;
        this.tournoi = tournoi;
    }

    @Override
    public String toString() {
        return "Classement=" + classement +
                ", tournoi=" + tournoi;
    }

    public int getClassement() {
        return classement;
    }

    public void setClassement(int classement) {
        this.classement = classement;
    }

    public Tournoi getTournoi() {
        return tournoi;
    }

    public void setTournoi(Tournoi tournoi) {
        this.tournoi = tournoi;
    }
}
