package metier;

//A completer !

import java.awt.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Joueur {

    //Attributs (et getters/setters) à définir.
    private String id_joueur;
    private LocalDateTime date_naissance;
    private String nom_joueur;
    private String prenom_joueur;
    private String ville_joueur;
    private Club club;
    private List<Resultat> listeResultat;

    public Joueur(String id_joueur, String nom_joueur, String prenom_joueur, LocalDateTime date_naissance, String ville_joueur, Club club, List<Resultat> listeResultat) {
        this.id_joueur = id_joueur;
        this.date_naissance = date_naissance;
        this.nom_joueur = nom_joueur;
        this.prenom_joueur = prenom_joueur;
        this.ville_joueur = ville_joueur;
        this.club = club;
        this.listeResultat = listeResultat;
    }

    public Joueur(String id_joueur, String nom_joueur, String prenom_joueur, LocalDateTime date_naissance, String ville_joueur, Club club) {
        this.id_joueur = id_joueur;
        this.date_naissance = date_naissance;
        this.nom_joueur = nom_joueur;
        this.prenom_joueur = prenom_joueur;
        this.ville_joueur = ville_joueur;
        this.club = club;
    }

    public Joueur(String id_joueur, String nom_joueur, String prenom_joueur, String ville_joueur, LocalDateTime date_naissance) {
        this.id_joueur = id_joueur;
        this.date_naissance = date_naissance;
        this.nom_joueur = nom_joueur;
        this.prenom_joueur = prenom_joueur;
        this.ville_joueur = ville_joueur;
    }

    @Override
    public String toString() {
        return "Joueur n°" + id_joueur +
                " né le " + date_naissance +
                "nommé " + nom_joueur + '\'' +
                " " + prenom_joueur + '\'' +
                "habitant à " + ville_joueur + '\'' +
                "au club " + club +
                " avec les résultats " + listeResultat;
    }


    public String getId_joueur() {
        return id_joueur;
    }

    public void setId_joueur(String id_joueur) {
        this.id_joueur = id_joueur;
    }

    public LocalDateTime getDate_naissance() {
        return this.date_naissance;
    }

    public int getAnneeDateNaissance() {
        return this.date_naissance.getYear();
    }

    public int getMoisDateNaissance() {
        return this.date_naissance.getMonthValue();
    }

    public int getJourDateNaissance() {
        return this.date_naissance.getDayOfMonth();
    }

    public void setDate_naissance(LocalDateTime date_naissance) {
        this.date_naissance = date_naissance;
    }

    public String getNom_joueur() {
        return nom_joueur;
    }

    public void setNom_joueur(String nom_joueur) {
        this.nom_joueur = nom_joueur;
    }

    public String getPrenom_joueur() {
        return prenom_joueur;
    }

    public void setPrenom_joueur(String prenom_joueur) {
        this.prenom_joueur = prenom_joueur;
    }

    public String getVille_joueur() {
        return ville_joueur;
    }

    public void setVille_joueur(String ville_joueur) {
        this.ville_joueur = ville_joueur;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public List<Resultat> getListeResultat() {
        return listeResultat;
    }

    public void setListeResultat(List<Resultat> listeResultat) {
        this.listeResultat = listeResultat;
    }
}
