package stockage;

import metier.Joueur;

import java.util.List;

public interface SourceMigration extends MigrateurInterface{
    public List<Joueur> recupererJoueurs();
}
