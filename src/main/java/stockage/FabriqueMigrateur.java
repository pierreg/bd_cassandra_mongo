package stockage;

import java.util.List;

public interface FabriqueMigrateur extends SourceMigration, DestinationMigration {
    public SourceMigration getSource();
    public DestinationMigration getDestination();
}
