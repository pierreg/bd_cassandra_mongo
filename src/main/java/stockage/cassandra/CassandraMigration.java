package stockage.cassandra;

import metier.Club;
import metier.Joueur;
import metier.Resultat;
import metier.Tournoi;
import stockage.DestinationMigration;
import stockage.FabriqueMigrateur;
import stockage.SourceMigration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CassandraMigration implements FabriqueMigrateur {

    private ConnexionCassandra connexionCassandra;

    private Connection connection;

    private Statement st;

    public CassandraMigration() {
        this.connexionCassandra = new ConnexionCassandra();
    }

    /**
     * Permet au programme d'initialiser la connexion avec la base Cassandra.
     */
    @Override
    public void connexion() {
        try {
            connection = this.connexionCassandra.ouvrirConnexion();
            st = connection.createStatement();
            System.out.println("Cassandra connecté");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Permet au programme de fermer la connexion avec la base Cassandra.
     */
    @Override
    public void deconnexion() {
        try {
            if(!st.isClosed()) {
                st.close();
            }
            connection.close();
            System.out.println("Cassandra déconnectée");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Effectue une requête auprès de la base de données Cassandra afin de récupérer les données des joueurs.
     * Cette méthode assure la conversion des données récupérées auprès de la base en objets métiers de type "Joueur".
     * Pour chaque joueur, il faudra aussi récupérer ses résultats.
     * @return Une liste d'objets "Joueur" construits à partir des données récupérées auprès de la base de données Cassandra.
     */
    @Override
    public List<Joueur> recupererJoueurs() {
        List<Joueur> joueurList = new ArrayList<>();
        try (
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        "SELECT id_joueur, prenom_joueur, nom_joueur, date_naissance, ville_joueur, id_club, nom_club, ville_club "
                                + "FROM joueurs_par_id_club ");

        )
        {
            while (rs.next()) {
                LocalDateTime dateNaissance = rs.getTimestamp(4).toLocalDateTime();
                Club club = new Club(rs.getString(6), rs.getString(7), rs.getString(8));
                Joueur joueur = new Joueur(rs.getString(1), rs.getString(2), rs.getString(3), rs.getTimestamp(4).toLocalDateTime(), rs.getString(5)
            , club);
                List<Resultat> resultatList = recupererResultatsJoueur(joueur);
                joueur.setListeResultat(resultatList);
                joueurList.add(joueur);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return joueurList;
    }

    /**
     * Effectue une requête auprès de la base de données Cassandra afin de récupérer les résultats d'un joueur.
     * Cette méthode assure la conversion des données récupérées auprès de la base en objets métiers de type "Resultat".
     * @param joueur : le joueur dont on osuhaite récupérer les résultats.
     * @return Une liste d'objets "Joueur" construits à partir des données récupérées auprès de la base de données Cassandra.
     */
    private List<Resultat> recupererResultatsJoueur(Joueur joueur) {
        List<Resultat> listeResultats = new ArrayList<>();
        try (
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        "SELECT id_tournoi, nom_tournoi, classement "
                                + "FROM resultats_par_id_joueur WHERE id_joueur = '" + joueur.getId_joueur() + "'");

        )
        {
            while (rs.next()) {
                Tournoi tournoi = new Tournoi(rs.getString(1), rs.getString(2));
                Resultat resultat = new Resultat(rs.getInt(3), tournoi);
                listeResultats.add(resultat);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listeResultats;
    }

    @Override
    public void migrerJoueurs(List<Joueur> joueurs) {
        System.out.println("Migration des données vers Cassandra...");
        joueurs.forEach(j -> {
            String query = "INSERT INTO joueurs_par_id_club (id_joueur, nom_joueur, prenom_joueur, ville_joueur, date_naissance, id_club, nom_club, ville_club) VALUES ('"
                    + j.getId_joueur() + "', '" + j.getNom_joueur() + "', '" + j.getPrenom_joueur() + "', '" + j.getVille_joueur() + "', '"
                    + j.getDate_naissance() + "', '" + j.getClub().getId_club() + "', '" + j.getClub().getNom_club() + "', '"
                    + j.getClub().getVille_club() + "')";
            try {
                this.st.execute(query);
                j.getListeResultat().forEach(r -> ajouterResultat(j, r));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });
        System.out.println("Migration des données des joueurs vers Cassandra effectuée.");
    }

    private void ajouterResultat(Joueur j, Resultat r) {
        String query = "INSERT INTO resultats_par_id_joueur (id_joueur, id_tournoi, classement, nom_joueur, nom_tournoi, prenom_joueur) VALUES ('"
                + j.getId_joueur() + "', '" + r.getTournoi().getId_tournois() + "', " +  r.getClassement() + ", '"
                + j.getNom_joueur() + "', '" + r.getTournoi().getNom_tournois() + "', '" + j.getPrenom_joueur() + "');";
        try {
            this.st.execute(query);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public SourceMigration getSource() {
        return this;
    }

    @Override
    public DestinationMigration getDestination() {
        return this;
    }

    public String getName() {
        return "Cassandra";
    }
}
