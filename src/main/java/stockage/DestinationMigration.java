package stockage;

import metier.Joueur;

import java.util.List;

public interface DestinationMigration extends MigrateurInterface{
    public void migrerJoueurs(List<Joueur> joueurs);
}
