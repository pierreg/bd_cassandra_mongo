package stockage.mongo;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import metier.Club;
import metier.Joueur;
import metier.Resultat;
import metier.Tournoi;
import org.bson.Document;
import stockage.DestinationMigration;
import stockage.FabriqueMigrateur;
import stockage.SourceMigration;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MongoMigration implements FabriqueMigrateur {

    private ConnexionMongoDB connexionMongoDB;
    private MongoClient client;

    protected MongoDatabase database;
    public MongoMigration() {
        this.connexionMongoDB = new ConnexionMongoDB();
    }

    /**
     * Permet au programme d'initialiser la connexion avec la base MongoDB.
     */
    public void connexion() {
        client = connexionMongoDB.ouvrirConnexion();
        database = client.getDatabase(connexionMongoDB.getDatabaseName());
        System.out.println("MongoDB connecté");
    }

    /**
     * Permet au programme de fermer la connexion avec la base MongoDB.
     */
    public void deconnexion() {
        client.close();
        System.out.println("MongoDB déconnecté");
    }

    /**
     * Migre (écrit) une liste d'objets "Joueur" dans la base de données Mongo en effectuant diverses requêtes.
     * Cette méthode assure la conversion des données contenues dans les objets en différents documents.
     * Les résultats des joueurs doivent aussi être migrés.
     * @param joueurs : liste de joueurs à migrer vers la base de données Mongo.
     */
    public void migrerJoueurs(List<Joueur> joueurs) {
        System.out.println("Migration des données vers Mongo...");
        for (Joueur j : joueurs) {
            Document date = new Document()
                    .append("jour", j.getJourDateNaissance())
                    .append("mois", j.getMoisDateNaissance())
                    .append("annee", j.getAnneeDateNaissance());
            Document club = new Document()
                    .append("id", j.getClub().getId_club())
                    .append("nom", j.getClub().getNom_club())
                    .append("ville", j.getClub().getVille_club());
            Document joueur = new Document()
                    .append("_id", j.getId_joueur())
                    .append("nom", j.getNom_joueur())
                    .append("prenom", j.getPrenom_joueur())
                    .append("dateNaissance", date)
                    .append("ville", j.getVille_joueur())
                    .append("club", club);
            List<Document> res = new ArrayList<>();
            for (Resultat r : j.getListeResultat()) {
                Document resultat = new Document();

                // Vérification et ajout de idTournoi s'il n'est pas null
                if (r.getTournoi() != null && r.getTournoi().getId_tournois() != null) {
                    resultat.put("idTournoi", r.getTournoi().getId_tournois());
                }

                // Vérification et ajout de nomTournoi s'il n'est pas null
                if (r.getTournoi() != null && r.getTournoi().getNom_tournois() != null) {
                    resultat.put("nomTournoi", r.getTournoi().getNom_tournois());
                }

                // Vérification et ajout de classement s'il n'est pas null
                if (r.getClassement() > 0) {
                    resultat.put("classement", r.getClassement());
                }

                res.add(resultat);
            }
            joueur.put("resultats", res);
            database.getCollection("joueurs").insertOne(joueur);
        }
        System.out.println("Migration des données des joueurs vers Mongo effectuée.");
    }

    @Override
    public SourceMigration getSource() {
        return null;
    }

    @Override
    public DestinationMigration getDestination() {
        return null;
    }

    @Override
    public List<Joueur> recupererJoueurs() {
        System.out.println("Récupération des joueurs depuis Mongo...");
        MongoCollection<Document> collection = this.database.getCollection("joueurs");
        List<Joueur> joueurs = new ArrayList<>();
        collection.find().forEach(j -> {
            LocalDateTime dateNaissance = LocalDateTime.of(
                    j.get("dateNaissance", Document.class).getInteger("annee"),
                    j.get("dateNaissance", Document.class).getInteger("mois"),
                    j.get("dateNaissance", Document.class).getInteger("jour"),
                    0, 0);

            Joueur joueur = new Joueur (
                    j.getString("_id"),
                    j.getString("nom"),
                    j.getString("prenom"),
                    j.getString("ville"),
                    dateNaissance
            );

            Document clubDocument = j.get("club", Document.class);
            if (clubDocument != null)
                joueur.setClub(new Club(clubDocument.getString("id"),
                        clubDocument.getString("nom"),
                        clubDocument.getString("ville")));

            List<Resultat> resultats = recupererResultats(j);
            joueur.setListeResultat(resultats);

            joueurs.add(joueur);
        });
        System.out.println("Récupération des joueurs depuis Mongo effectuée.");
        return joueurs;
    }
    private List<Resultat> recupererResultats(Document joueurDocument) {
        List<Resultat> resultats = new ArrayList<>();
        List<Document> resultatsDocument = joueurDocument.get("resultats", List.class);
        if (resultatsDocument != null)
            resultatsDocument.forEach(r -> {
                if (r.getInteger("classement") == null) {
                    resultats.add(new Resultat(0,
                            new Tournoi(r.getString("idTournoi"), r.getString("nomTournoi"))));
                }else if (r.getString("idTournoi") == null) {
                    resultats.add(new Resultat(r.getInteger("classement"), null));
                }
                else {
                    resultats.add(new Resultat(r.getInteger("classement"),
                            new Tournoi(r.getString("idTournoi"), r.getString("nomTournoi"))));
                }
            });
        return resultats;
    }

    public String getName() {
        return "MongoDB";
    }

}
