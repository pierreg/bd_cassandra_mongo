import application.service.ServiceMigration;
import metier.Joueur;
import stockage.cassandra.CassandraMigration;
import stockage.cassandra.ConnexionCassandra;
import stockage.mongo.ConnexionMongoDB;
import stockage.mongo.MongoMigration;

import java.util.List;

public class Main {

    public static void migrationCassandraMongo() {

        //Etape 1 : Connexion à Cassandra

        ConnexionCassandra connexionCassandra = new ConnexionCassandra();
        connexionCassandra.ouvrirConnexion();
        CassandraMigration cassandraSourceMigration = new CassandraMigration(connexionCassandra);
        cassandraSourceMigration.connexion();

        //Etape 2 : Récupération des joueurs (et de leurs résultats) depuis Cassandra
        List<Joueur> joueurList = cassandraSourceMigration.recupererJoueurs();

        //Etape 3 : Déconnexion de Cassandra
        cassandraSourceMigration.deconnexion();
        System.out.println("\nJoueurs récupérés depuis la source : ");

        //Etape 4 : Affichage des données des joueurs récupérés
        for (Joueur joueur : joueurList) {
            System.out.println(joueur.toString());
        }

        //Etape 5 : Connexion à MongoDB
        ConnexionMongoDB connexionMongoDB = new ConnexionMongoDB();
        connexionMongoDB.ouvrirConnexion();
        MongoMigration mongoDestinationMigration = new MongoMigration(connexionMongoDB);
        mongoDestinationMigration.connexion();

        //Etape 6 : Migration des données des joueurs vers MongoDB
        mongoDestinationMigration.migrerJoueurs(joueurList);

        //Etape 7 : Déconnexion de MongoDB
        mongoDestinationMigration.deconnexion();
    }

    public static void main(String[] args) {

       ServiceMigration serviceMigration = new ServiceMigration();
       serviceMigration.effectuerMigration(new MongoMigration(), new CassandraMigration());
    }

}
