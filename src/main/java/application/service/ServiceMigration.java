package application.service;

import metier.Joueur;
import stockage.DestinationMigration;
import stockage.SourceMigration;

import java.util.List;

public class ServiceMigration implements SourceMigration, DestinationMigration{


    public void effectuerMigration(SourceMigration source, DestinationMigration destination) {
        System.out.println("Migration des données de "+ source.getClass().getName() + " vers " + destination.getClass().getName());
        source.connexion();
        List<Joueur> joueurList = source.recupererJoueurs();
        source.deconnexion();
        for (Joueur joueur : joueurList) {
            System.out.println(joueur.toString());
        }
        destination.connexion();
        destination.migrerJoueurs(joueurList);
        destination.deconnexion();

    }

    @Override
    public void migrerJoueurs(List<Joueur> joueurs) {

    }

    @Override
    public void connexion() {

    }

    @Override
    public void deconnexion() {

    }

    @Override
    public List<Joueur> recupererJoueurs() {
        return null;
    }
}
